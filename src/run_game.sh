#!/bin/sh

javac ~/Playground/IdeaProjects/HaliteBotV6/src/*.java ~/Playground/IdeaProjects/HaliteBotV6/src/Lazuq/*.java
javac *.java Lazuq/*.java 

#./halite -d "240 160" "java GLazuV8" "java MyBot"
if [[ uname -eq "Darwin" ]]; then
    ./halite-mac -d "384 256" "java GLazuV8" "java -cp ~/Playground/IdeaProjects/HaliteBotV6/src MyBotv6"
else
    ./halite -d "384 256" "java GLazuV8" "java -cp ~/Playground/IdeaProjects/HaliteBotV6/src MyBotv6"
fi
