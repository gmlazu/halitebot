package Lazuq;

import hlt.GameMap;
import hlt.Log;
import hlt.Ship;

public class TestStrategy extends Strategy {

    /**
     * Constructor for the BalancedStrategy class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public TestStrategy(GameMap gameMap, Plan plan) {
        super(gameMap, plan);
    }

    /**
     * This method initializes the current strategy by sending
     * 1/4 ships to attack, and 3/4 ships to mine.
     */
    @Override
    public void init() {
        int shipCount = 3;
        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
            // Docked ships are ignored
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            if (shipCount % 3 == 0) {
                if (!sendShipScrewOpponent(ship)) {
                    boolean shipAttacked = sendShipToAttack(ship, false);
                    Log.log("Ship failed to screw, sent to attack");
                    Log.log("Sent ship to attack, status is: " + shipAttacked);
                } else {
                    Log.log("Ship was sent to screw");
                }
            } else {
                if (!sendShipToMine(ship, true)) {
                    boolean shipAttacked = sendShipToAttack(ship, true);
                    Log.log("Failed to send ship to mine, send to attack.");
                    Log.log("Sent ship to attack, status is: " + shipAttacked);
                } else {
                    Log.log("Sent ship to mine.");
                }
            }

            shipCount++;
        }
    }
}