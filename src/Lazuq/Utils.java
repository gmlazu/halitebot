package Lazuq;

import hlt.*;

import java.awt.geom.Line2D;
import java.util.*;

/**
 * Static class of utility functions.
 */
public class Utils {

    /**
     * Returns an ArrayList of Planets containing all owned (non-neutral) planets.
     *
     * @param gameMap the game map
     * @return an ArrayList of owned Planets
     */
    public static ArrayList<Planet> getAllOwnedPlanets(GameMap gameMap) {
        ArrayList<Planet> ownedPlanets = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            if (planet.isOwned()) ownedPlanets.add(planet);
        }
        return ownedPlanets;
    }

    /**
     * Returns an ArrayList of Planets containing all of your owned planets.
     *
     * @param gameMap the game map
     * @return an ArrayList of owned Planets
     */
    public static ArrayList<Planet> getMyOwnedPlanets(GameMap gameMap) {
        ArrayList<Planet> ownedPlanets = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            if (planet.isOwned() && planet.getOwner() == gameMap.getMyPlayerId())
                ownedPlanets.add(planet);
        }

        return ownedPlanets;
    }

    /**
     * Returns an ArrayList of Planets containing all neutral (not owned) planets.
     *
     * @param gameMap the game map
     * @return an ArrayList of neutral planets
     */
    public static ArrayList<Planet> getAllNeutralPlanets(GameMap gameMap) {
        ArrayList<Planet> neutralPlanets = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            if (!planet.isOwned()) neutralPlanets.add(planet);
        }
        return neutralPlanets;
    }

    /**
     * This helper function returns the available docking spots for the specified planet.
     *
     * @param planet the planet for which you want the available docking spots
     * @return available docking spots
     */
    public static int getAvailableDockingSpots(Planet planet) {
        return planet.getDockingSpots() - planet.getDockedShips().size();
    }

    /**
     * This helper function returns the number of average docking spots across the map
     *
     * @param gameMap the game map
     * @return the average docking spots
     */
    public static int getAverageDockingSpots(GameMap gameMap) {
        double averageDockingSpots = 0;
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            averageDockingSpots += planet.getDockingSpots();
        }

        return (int) Math.ceil(averageDockingSpots / gameMap.getAllPlanets().values().size());
    }

    /**
     * This helper function returns the number of average docking spots across the map
     *
     * @param gameMap the game map
     * @return the average docking spots
     */
    public static double getAverageDistanceBetweenPlanets(GameMap gameMap) {
        double avgDistance = 0;

        for (Planet planet : gameMap.getAllPlanets().values()) {
            for (Entity nearbyEntity : gameMap.nearbyEntitiesByDistance(planet).values()) {
                // we skip if entity is not a Ship
                if (!(nearbyEntity instanceof Planet)) continue;

                Planet closestPlanet = (Planet) nearbyEntity;

                avgDistance += planet.getDistanceTo(new Position(closestPlanet.getXPos(), closestPlanet.getYPos()));
            }
        }

        return avgDistance / gameMap.getAllPlanets().size();
    }

    public static int getAverageShipHealth(GameMap gameMap) {
        int avgShipHealth = 0;

        Collection<Ship> ships = gameMap.getMyPlayer().getShips().values();

        for (Ship ship : ships) {
            avgShipHealth += ship.getHealth();
        }

        return (int) Math.ceil(avgShipHealth / ships.size());
    }

    /**
     *
     * @param gameMap the game map
     * @param ship    the ship from which to calculate the distance
     * @param type    0: all kinds of enemy ships; 1: docked enemy ships; 2: undocked enemy ships; 3: docked enemy ships and undocked enmy ships, in that order
     * @return the enemy ships
     */
    public static ArrayList<Ship> getEnemyShipsByDistance(GameMap gameMap, Ship ship, int type) {
        return getShipsByDistance(gameMap, ship, type, false);
    }

    public static ArrayList<Ship> getAllyShipsByDistance(GameMap gameMap, Ship ship, int type) {
        return getShipsByDistance(gameMap, ship, type, true);
    }

    /**
     * This helper function returns a list of ships ordered by distance
     *
     * @param gameMap the game map
     * @param ship    the ship from which to calculate the distance
     * @param type    0: all kinds of enemy ships; 1: docked enemy ships; 2: undocked enemy ships; 3: docked enemy ships and undocked enmy ships, in that order
     * @return the ships list
     */
    private static ArrayList<Ship> getShipsByDistance(GameMap gameMap, Ship ship, int type, boolean mine) {
        ArrayList<Ship> enemyShips = new ArrayList<>();

        // if type is 3, then we need to return first docked ships then undocked ships, in that order
        if (type == 3) {
            enemyShips.addAll(getShipsByDistance(gameMap, ship, 1, mine));
            enemyShips.addAll(getShipsByDistance(gameMap, ship, 2, mine));
        } else {
            for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
                // we skip if entity is not a Ship
                if (!(entity instanceof Ship))
                    continue;

                Ship enemyShip = (Ship) entity;

                if (mine ? enemyShip.getOwner() == gameMap.getMyPlayerId() : enemyShip.getOwner() != gameMap.getMyPlayerId()) {
                    if (type == 0 ||
                            (type == 1 && (enemyShip.getDockingStatus() == Ship.DockingStatus.Docked || enemyShip.getDockingStatus() == Ship.DockingStatus.Docking)) ||
                            (type == 2 && enemyShip.getDockingStatus() == Ship.DockingStatus.Undocked))
                        enemyShips.add(enemyShip);
                }
            }
        }
        return enemyShips;
    }

    /**
     * This helper function returns an ArrayList of enemy planets ordered by distance
     *
     * @param gameMap the game map
     * @param ship    the ship from which to calculate the distance
     * @return the enemy planets
     */
    public static ArrayList<Planet> getEnemyPlanetsByDistance(GameMap gameMap, Ship ship) {
        ArrayList<Planet> enemyPlanets = new ArrayList<>();

        for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
            // we skip if entity is not a Ship
            if (!(entity instanceof Planet))
                continue;

            Planet enemyPlanet = (Planet) entity;

            if (enemyPlanet.getOwner() != gameMap.getMyPlayerId()) {
                enemyPlanets.add(enemyPlanet);
            }
        }
        return enemyPlanets;
    }

    /**
     * This helper function returns a list of enemy ships within a certain radius
     *
     * @param gameMap the game map
     * @param entity  the entity from which to calculate the distance
     * @param radius  the radius around the entity
     * @return the nearby entities
     */
    public static ArrayList<Ship> getEnemyShipsWithinRadius(GameMap gameMap, Entity entity, double radius) {
        ArrayList<Ship> nearbyShips = new ArrayList<>();

        for (Entity nearbyEntity : gameMap.nearbyEntitiesByDistance(entity).values()) {
            // we skip if entity is not a Ship
            if (!(nearbyEntity instanceof Ship)) continue;

            Ship nearbyShip = (Ship) nearbyEntity;
            if (nearbyShip.getOwner() == gameMap.getMyPlayerId()) continue;

            if (entity.getDistanceTo(new Position(nearbyShip.getXPos(), nearbyShip.getYPos())) <= radius) {
                nearbyShips.add(nearbyShip);
            }
        }

        return nearbyShips;
    }

    /**
     * This helper function returns a list of ally ships within a certain radius
     *
     * @param gameMap the game map
     * @param entity  the entity from which to calculate the distance
     * @param radius  the radius around the entity
     * @return the nearby entities
     */
    public static ArrayList<Ship> getAllyShipsWithinRadius(GameMap gameMap, Entity entity, double radius) {
        ArrayList<Ship> nearbyShips = new ArrayList<>();

        for (Entity nearbyEntity : gameMap.nearbyEntitiesByDistance(entity).values()) {
            // we skip if entity is not a Ship
            if (!(nearbyEntity instanceof Ship)) continue;

            Ship nearbyShip = (Ship) nearbyEntity;
            if (nearbyShip.getOwner() != gameMap.getMyPlayerId()) continue;

            if (entity.getDistanceTo(new Position(nearbyShip.getXPos(), nearbyShip.getYPos())) <= radius) {
                nearbyShips.add(nearbyShip);
            }
        }

        return nearbyShips;
    }

    public static Position getPositionInFront(Entity entity, int distance, int angle) {
        double radAngle = Math.toRadians(angle);
        double x = entity.getXPos() + distance * Math.cos(radAngle);
        double y = entity.getYPos() + distance * Math.sin(radAngle);

        return new Position(x, y);
    }

    public static Line2D getShipMovementForecast(Entity entity, int orientAngle) {
        float currentPosXPos = (float) entity.getXPos();
        float currentPosYPos = (float) entity.getYPos();

        Position forecastPos = getPositionInFront(entity, Constants.MAX_SPEED, orientAngle);
        float forecastPosXPos = (float) forecastPos.getXPos();
        float forecastPosYPos = (float) forecastPos.getYPos();

        return new Line2D.Float(currentPosXPos, currentPosYPos, forecastPosXPos, forecastPosYPos);
    }

    public static HashMap<Integer, Integer> findCollisions(GameMap gameMap, HashMap<Integer, Move> shipMovements) {
        HashMap<Integer, Integer> shipsColliding = new HashMap<>();

        Iterator iter = shipMovements.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry plan = (Map.Entry) iter.next();
            Integer shipID = (Integer) plan.getKey();

            Ship ship = gameMap.getMyPlayer().getShip(shipID);
            Move move = (Move) plan.getValue();


            // if the ship is docked or docking, it cannot collide.
            if (ship.getDockingStatus() == Ship.DockingStatus.Docked ||
                    ship.getDockingStatus() == Ship.DockingStatus.Docking ||
                    shipMovements.get(ship.getId()) instanceof DockMove)
                continue;

            // we get our ship's move
            ThrustMove shipMove = (ThrustMove) move;
            int shipOrientation = shipMove.getAngle();

            Line2D shipForecast = getShipMovementForecast(ship, shipOrientation);

            for (Ship allyShip : getAllyShipsWithinRadius(gameMap, ship, Constants.MAX_SPEED)) {
                // an ally ship is not elligible for collision testing if it is docked,
                // docking, or planned to dock.
                if (allyShip.getDockingStatus() == Ship.DockingStatus.Docked ||
                        allyShip.getDockingStatus() == Ship.DockingStatus.Docking ||
                        shipMovements.get(allyShip.getId()) instanceof DockMove)
                    continue;

                // we get ally ship's move
                ThrustMove allyShipMove = (ThrustMove) shipMovements.get(allyShip.getId());

                // if a ship could not be given a proper navigation,
                // it will have no moves.
                if (allyShipMove == null) {
                    continue;
                }

                int allyShipOrientation = allyShipMove.getAngle();
                Line2D allyShipForecast = getShipMovementForecast(allyShip, allyShipOrientation);

                Log.log("Ally ship found within 7 units");
                if (shipForecast.intersectsLine(allyShipForecast) && shipsColliding.get(allyShip.getId()) == null) {
                    Log.log("Ship likely to collide.");
                    shipsColliding.put(ship.getId(), allyShip.getId());
                }
            }
        }

        return shipsColliding;
    }

    public static boolean outOfBounds(Position target, GameMap map) {
        if (target.getXPos() < 7.0 || target.getXPos() > map.getWidth() - 7 || target.getYPos() < 7.0 || target.getYPos() > map.getHeight() - 7) {
            return true;
        }
        return false;
    }
}
