package Lazuq;

import hlt.*;

import java.util.*;

/**
 * This class allows you to create a plan for this round, and obtain
 * a move list to send to Networking.sendMoves()
 */
public class Plan {

    /**
     * The Game Map.
     */
    private GameMap gameMap;

    private HashMap<Integer, Move> shipMoves;

    /**
     * This HashMap holds a planet and the Ships that are docking to it.
     */
    private HashMap<Integer, ArrayList<Integer>> planetsBeingDocked;

    /**
     * This HashMap holds Ships and the planets they are docking to.
     */
    private HashMap<Integer, Integer> shipsDocking;

    /**
     * This HashMap holds a Ship ID of our ship, and the Ship it is attacking.
     */
    private HashMap<Integer, Ship> shipsAttacking;

    /**
     * This HashMap holds an enemy Ship, and an ArrayList of Ships that are attacking it.
     */
    private HashMap<Integer, ArrayList<Integer>> shipsAttacked;

    /**
     * This ArrayList holds the moves that need to be sent to the Networking class
     */
    private ArrayList<Move> moveList;

    /**
     * Constructor for the Plan class
     * @param gameMap the Game Map
     */
    public Plan(GameMap gameMap) {
        this.shipMoves = new HashMap<>();
        this.planetsBeingDocked = new HashMap<>();
        this.shipsDocking = new HashMap<>();
        this.shipsAttacking = new HashMap<>();
        this.shipsAttacked = new HashMap<>();
        this.moveList = new ArrayList<>();
        this.gameMap = gameMap;
    }

    /**
     * Attempt to schedule a ship to dock to a planet
     * @param ship the ship to dock
     * @param planet the planet to dock to
     * @return true if succeeded, false if failed
     */
    public boolean scheduleDock(Ship ship, Planet planet) {
        // Get ships we are competing with to dock a planet
        Ship dockCompetition = getDockCompetition(ship, planet);

        // If we have competition, we attack it
        if (dockCompetition != null) {
            shipsDocking.remove(ship.getId());

            ArrayList<Integer> shipsDockingToPlanet = planetsBeingDocked.get(planet.getId());

            if (shipsDockingToPlanet != null && shipsDockingToPlanet.contains(ship.getId())) {
                shipsDockingToPlanet.remove(shipsDockingToPlanet.indexOf(ship.getId()));
            }

            scheduleAttack(ship, dockCompetition);
            return true;
        }

        // If the ship is already scheduled to dock
        // or attack, we return true
        if (shipsDocking.get(ship.getId()) != null ||
                shipsAttacking.get(ship.getId()) != null) {
            return true;
        }

        // We initialize an empty ArrayList, if absent, to store the ships docking to a planet
        planetsBeingDocked.computeIfAbsent(planet.getId(), k -> new ArrayList<>());

        // We cannot dock planets that are owned by other players,
        // planets that are full,
        // or planets which are full in our plan
        if ((planet.getOwner() != gameMap.getMyPlayerId() && planet.isOwned()) ||
                planet.isFull() ||
                planetsBeingDocked.get(planet.getId()).size() >= Utils.getAvailableDockingSpots(planet)) {
            return false;
        }

        // Lastly, if none of the above happen, we schedule docking
        planetsBeingDocked.get(planet.getId()).add(ship.getId());
        shipsDocking.put(ship.getId(), planet.getId());
        return true;
    }

    /**
     * Attempt to schedule a ship to attack another ship
     * @param ship the ship to send to attack
     * @param enemyShip the ship attacked
     * @return true if succeeded, false if failed
     */
    public boolean scheduleAttack(Ship ship, Ship enemyShip) {
        // If the ship is already scheduled to
        // attack or dock, we return true
        if (shipsDocking.get(ship.getId()) != null ||
                shipsAttacking.get(ship.getId()) != null) {
            return true;
        }

        // We initialize an empty ArrayList, if absent, to store the ships attacking an enemy ship
        shipsAttacked.computeIfAbsent(enemyShip.getId(), k -> new ArrayList<>());

        // Determine the ratio between my ships and enemy ships
        double myNumberOfShips = gameMap.getMyPlayer().getShips().size();
        double numberOfEnemyShips = gameMap.getAllShips().size() - myNumberOfShips;
        double shipToShipRatio = Math.ceil(myNumberOfShips / numberOfEnemyShips);

        // The max no. of ships that can attack any enemy ship is
        // the ratio between my ships and enemy ships
        if (shipsAttacked.get(enemyShip.getId()).size() < shipToShipRatio) {
            shipsAttacking.put(ship.getId(), enemyShip);
            shipsAttacked.get(enemyShip.getId()).add(ship.getId());
            return true;
        } else {
            return false;
        }
    }

    public Position Strafe(Ship ship, Ship target, Ship hunter) {
        int angle_deg = ship.orientTowardsInDeg(hunter);

        double left_dx = Math.cos(angle_deg * (Math.PI / 180) + (Math.PI / 2.0)) * 7.0;
        double left_dy = Math.sin(angle_deg * (Math.PI / 180) + (Math.PI / 2.0)) * 7.0;
        double right_dx = Math.cos(angle_deg * (Math.PI / 180) - (Math.PI / 2.0)) * 7.0;
        double right_dy = Math.sin(angle_deg * (Math.PI / 180) - (Math.PI / 2.0)) * 7.0;

        Position left_strafe = new Position(ship.getXPos() + left_dx, ship.getYPos() + left_dy);
        Position right_strafe = new Position(ship.getXPos() + right_dx, ship.getYPos() + right_dy);

        if (left_strafe.getDistanceTo(target) <= right_strafe.getDistanceTo(target)) {
            return left_strafe;
        }
        return right_strafe;
    }

    protected boolean thrustShipToPosition(Ship ship, final Position position) {
        Position destination = position;

        // create a new thrust move to the target
        ThrustMove newThrustMove = Navigation.navigateShipTowardsTarget(
                gameMap,
                ship,
                destination,
                Constants.MAX_SPEED,
                true,
                Constants.MAX_NAVIGATION_CORRECTIONS,
                Math.PI / 180.0
        );

        if (newThrustMove == null) {
            return false;
        }

        int angle = newThrustMove.getAngle();
        while (Utils.outOfBounds(destination, gameMap)) {
            destination = Utils.getPositionInFront(ship, Constants.MAX_SPEED, angle % 360);
            angle += 10;
        }

        if (destination != position) {
            newThrustMove = Navigation.navigateShipTowardsTarget(
                    gameMap,
                    ship,
                    destination,
                    Constants.MAX_SPEED,
                    true,
                    Constants.MAX_NAVIGATION_CORRECTIONS,
                    Math.PI / 180.0
            );

            if (newThrustMove == null) {
                return false;
            }
        }

        shipMoves.put(ship.getId(), newThrustMove);
        return true;
    }

    /**
     * Determine if there is competition for docking a planet.
     * This helper function determines if there's a ships going
     * towards a planet, closer to the planet than us.
     * @param ship my ship
     * @param planet the planet targeted for docking
     * @return the Ship object considered competition if found. Otherwise, null.
     */
    private Ship getDockCompetition(Ship ship, Planet planet) {
        // iterate through every entity, ordered by distance
        for (Entity entity : gameMap.nearbyEntitiesByDistance(planet).values()) {
            // we skip if entity is not a Ship
            if (!(entity instanceof Ship)) continue;
            Ship enemyShip = (Ship) entity;

            // or if the ship is ours
            if (enemyShip.getOwner() == gameMap.getMyPlayerId()) continue;

            // if the enemy ship is oriented towards our targeted planet,
            // +/- 10 deg, and is closer than our ship, we return that ship
            if (enemyShip.orientTowardsInDeg(new Position(planet.getXPos(), planet.getYPos())) <= 10 &&
                    ship.getDistanceTo(planet) > enemyShip.getDistanceTo(planet)) {
                return enemyShip;
            }
        }

        // if no ships are going towards our target planet, return null
        return null;
    }

    /**
     * This method checks whether events we had planned occured,
     * or whether an event should be changed, and removes or
     * changes these events.
     */
    public void updatePlan() {
        // clear the moveLists from the previous round
        moveList.clear();
        shipMoves.clear();

        // iterate through docking plans
        Iterator<Map.Entry<Integer, Integer>> shipsDockingIterator = shipsDocking.entrySet().iterator();
        while (shipsDockingIterator.hasNext()) {
            Map.Entry<Integer, Integer> plan = shipsDockingIterator.next();
            Integer shipID = plan.getKey();
            Integer planetID = plan.getValue();

            Ship ship = gameMap.getMyPlayer().getShip(shipID);
            Planet planet = gameMap.getPlanet(planetID);

            // if our ship is dead, docked, or the planet it was after was docked
            // by someone else, remove these plans
            if (ship == null ||
                    ship.getDockingStatus() == Ship.DockingStatus.Docked ||
                    ship.getDockingStatus() == Ship.DockingStatus.Docking ||
                    planet.getOwner() != gameMap.getMyPlayerId()) {
                shipsDockingIterator.remove();
                planetsBeingDocked.get(planet.getId()).remove(shipID);

                continue;
            }

            // if we have an enemy ship within 50 distance from us, attack it instead
            ArrayList<Ship> nearbyShips = Utils.getEnemyShipsWithinRadius(gameMap, ship, 50);

            if (nearbyShips.size() > 0) {
                shipsDockingIterator.remove();
                planetsBeingDocked.get(planet.getId()).remove(shipID);

                scheduleAttack(ship, nearbyShips.get(0));
            }
        }

        // iterate through attack plans
        Iterator<Map.Entry<Integer, Ship>> shipsAttackingIterator = shipsAttacking.entrySet().iterator();
        while (shipsAttackingIterator.hasNext()) {
            Map.Entry<Integer, Ship> plan = shipsAttackingIterator.next();
            Integer shipID = plan.getKey();

            Ship ship = gameMap.getMyPlayer().getShip(shipID);
            Ship enemyShipObj = plan.getValue();

            try {
                // if the enemy ship is dead, or our ship is dead, remove plans
                Ship enemyShip = gameMap.getShip(enemyShipObj.getOwner(), enemyShipObj.getId());
                if (enemyShip == null || ship == null) {
                    shipsAttackingIterator.remove();
                    shipsAttacked.remove(enemyShipObj.getId());
                }
            } catch (IndexOutOfBoundsException e) {
                // if we catch an IndexOutOfBoundsException,
                // it means that the player is dead.
                shipsAttackingIterator.remove();
                shipsAttacked.remove(enemyShipObj.getId());
            }

//            // if we have an enemy ship within 50 distance from us, attack it instead
//            ArrayList<Ship> nearbyShips = Utils.getEnemyShipsWithinRadius(gameMap, ship, 50);
//
//            if (nearbyShips.size() > 0) {
//                iter.remove();
//                shipsAttacked.remove(enemyShipObj.getId());
//
//                scheduleAttack(ship, nearbyShips.get(0));
//            }
        }
    }

    private void populateMoves() {
        // iterate thrgough planned dockings
        Iterator<Map.Entry<Integer, Integer>> shipsDockingIterator = shipsDocking.entrySet().iterator();
        while (shipsDockingIterator.hasNext()) {
            Map.Entry<Integer, Integer> plan = shipsDockingIterator.next();
            Integer shipID = plan.getKey();
            Integer planetID = plan.getValue();

            // fetch the ship and planet based on IDs
            Ship ship = gameMap.getMyPlayer().getShip(shipID);
            Planet planet = gameMap.getPlanet(planetID);

            // create a ThrustMove to the target
            ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet, Constants.MAX_SPEED);

            if (newThrustMove != null) {
                // Verify whether we can dock.
                // If we can, dock, otherwise, move to the target
                if (ship.canDock(planet)) {
                    shipMoves.put(ship.getId(), new DockMove(ship, planet));
                } else {
                    shipMoves.put(ship.getId(), newThrustMove);
                }
            } else {
                // What should we do in this case? Ship will stop for one round.
                LogBlana.log("Plan::getMoveList() > newThrustMove was null.");
            }
        }

        // iterate through planned attacks
        Iterator<Map.Entry<Integer, Ship>> shipsAttackingIterator = shipsAttacking.entrySet().iterator();
        while (shipsAttackingIterator.hasNext()) {
            Map.Entry<Integer, Ship> plan = shipsAttackingIterator.next();
            Integer shipID = plan.getKey();
            Ship enemyShipObj = plan.getValue();

            // get the ship and enemy ship based on IDs
            Ship ship = gameMap.getMyPlayer().getShip(shipID);
            Ship enemyShip = gameMap.getShip(enemyShipObj.getOwner(), enemyShipObj.getId());

            // create a new thrust move to the target
            ThrustMove newThrustMove = Navigation.navigateShipTowardsTarget(
                    gameMap,
                    ship,
                    new Position(enemyShip.getXPos(), enemyShip.getYPos()),
                    Constants.MAX_SPEED,
                    true,
                    Constants.MAX_NAVIGATION_CORRECTIONS,
                    Math.PI / 180.0
            );

            if (newThrustMove != null) {
                shipMoves.put(ship.getId(), newThrustMove);
            }
        }
    }

    /**
     * Getter for the planetsBeingDocked instance variable.
     * This variable holds a HashMap containing a Planet, and an ArrayList of ships docking to that planet.
     * @return the planetsBeingDocked HashMap
     */
    public HashMap<Integer, ArrayList<Integer>> getPlanetsBeingDocked() {
        return planetsBeingDocked;
    }

    /**
     * Getter for the getShipsDocking instance variable.
     * This variable holds a HashMap containing Ships, and the Planets they are docking to.
     * @return the shipsDocking HashMap
     */
    public HashMap<Integer, Integer> getShipsDocking() {
        return shipsDocking;
    }

    /**
     * Getter for the shipsAttacking instance variable.
     * This variable holds a HashMap containing a Ship, and the Ship it is attacking.
     * @return the shipsAttacking HashMap
     */
    public HashMap<Integer, Ship> getShipsAttacking() {
        return shipsAttacking;
    }

    /**
     * Getter for the shipsAttacked instance variable.
     * This variable holds a HashMap containing a Ship, and an ArrayList of Ships
     * that are currently planned to attack it.
     * @return the shipsAttacked HashMap
     */
    public HashMap<Integer, ArrayList<Integer>> getShipsAttacked() {
        return shipsAttacked;
    }

    /**
     * Getter for the moveList instance variable.
     * This variable holds an ArrayList containing the moveList for the current Plan.
     * @return the moveList
     */
    public ArrayList<Move> getMoveList() {
        populateMoves();

        HashMap<Integer, Integer> collisions = Utils.findCollisions(gameMap, shipMoves);

        // iterate through moves
        Iterator<Map.Entry<Integer, Move>> iterator = shipMoves.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Move> plan = iterator.next();
            Integer shipID = plan.getKey();
            Move move = plan.getValue();

            Ship ship = gameMap.getMyPlayer().getShip(shipID);

            // Any other type of move other than ThrustMove cannot result in collision
            if (!(move instanceof ThrustMove)) {
                moveList.add(move);
                continue;
            }

            // Determine whether the ship will collide, and get the ship it will collide with
            Integer shipCollision = collisions.get(shipID);

            // if we can collide with a ship, stop this ship
            if (shipCollision != null) {
                Move noopMove = new Move(Move.MoveType.Noop, ship);
                moveList.add(noopMove);
            } else {
                moveList.add(move);
            }
        }

        return moveList;
    }
}
