package Lazuq;

import hlt.GameMap;
import hlt.Ship;

/**
 * The ConquerStrategy class extends the abstract class Strategy,
 * implementing a planet-conquering strategy upon init.
 */
public class ConquerStrategy extends Strategy {

    /**
     * Constructor for the Lazuq.ConquerStrategy class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public ConquerStrategy(GameMap gameMap, Plan plan) {
        super(gameMap, plan);
    }

    /**
     * This method initializes the current strategy by sending
     * undocked ships to mine. If this action fails, the ships are
     * instead sent to attack.
     */
    @Override
    public void init() {
        // iterate through every ship of ours
        for (Ship ship : gameMap.getMyPlayer().getShips().values()) {
            // skip docked ships
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            // attempt to send the ship to mine
            if (!sendShipToMine(ship, true)) {
                // if we failed to send this ship to mine, send it to attack
                sendShipToAttack(ship, true);
            }
        }
    }
}
