package Lazuq;

import hlt.GameMap;
import hlt.Log;

/**
 * Factory class to generate a type of Strategy to use this round.
 */
public class StrategyFactory {

    /**
     * Factory getter method to determine what strategy to use.
     * The AttackStrategy strategy will be chosen if there are no more neutral planets.
     * The ConquerStrategy strategy will be chosen if the game is 4-player.
     * The BalancedStrategy strategy will be chosen in any other case.
     * @param gameMap the game map
     * @param plan the main Plan object
     * @return the strategy to use this round
     */
    public static Strategy getStrategy(GameMap gameMap, Plan plan) {

        // if the game is single player, we implement the single player strategy
        if (gameMap.getAllPlayers().size() == 1) {
            Log.log("Using the SinglePlayerStrategy");
            return new SinglePlayerStrategy(gameMap, plan);
        }

        // if there are no more planets to conquer,
        // or if the game map has small planets, we attack
        if (Utils.getAllOwnedPlanets(gameMap).size() == gameMap.getAllPlanets().size() || (Utils.getAverageDistanceBetweenPlanets(gameMap) < 800)) {
            Log.log("Using the AttackStrategy");
            return new AttackStrategy(gameMap, plan);
        }

        // if the game is 4 player, or if the distance between planets is small,
        // we use the conquer strategy until there are no more planets to conquer
        if (gameMap.getAllPlayers().size() == 4) { // || (Utils.getAverageDistanceBetweenPlanets(gameMap) > 800 && gameMap.getWidth() > 300 && gameMap.getHeight() > 200)) {
            Log.log("Using the ConquerStrategy");
            return new ConquerStrategy(gameMap, plan);
        }

        // in every other case, use balanced strategy
        Log.log("Using the TestStrategy");
        return new TestStrategy(gameMap, plan);
    }
}
