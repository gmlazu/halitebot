package Lazuq;

import java.io.FileWriter;
import java.io.IOException;

/**
 * This is a very blanao class for logging.
 */
public class LogBlana {

    private static FileWriter file = null;

    /**
     * Log this into LogBlana.txt
     * @param log string to log
     */
    public static void log(String log) {
        try {
            file = new FileWriter("LogBlana.txt", true);
            file.write(log + System.getProperty("line.separator"));
            if (file != null) file.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
