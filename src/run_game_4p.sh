#!/bin/sh

javac MyBotv3.java
javac MyBotv4.java
javac MyBot.java
javac GLazuV8.java

if [[ uname -eq "Darwin" ]]; then
    ./halite-mac -d "240 160" "java GLazuV8" "java MyBot" "java MyBotv4" "java MyBotv3"
else
    ./halite -d "240 160" "java GLazuV8" "java MyBot" "java MyBotv4" "java MyBotv3"
fi
