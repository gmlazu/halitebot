import Lazuq.*;
import hlt.*;

/**
 * The 6th version of this bot
 */
public class GLazuV8 {

    public static void main(final String[] args) {

        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("GLazuV8");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);

        // The plan for the game. will be updated every round with actions pending
        Plan plan = new Plan(gameMap);

        for (;;) {
            networking.updateMap(gameMap);

            LogBlana.log("~~~~ A new round has started. ~~~~");

            // we update the plan based on the current game status
            plan.updatePlan();

            // generate the strategy for this round, and execute it
            Strategy strategy = StrategyFactory.getStrategy(gameMap, plan);
            strategy.init();

            // send the move list from the strategy to Networking.
            Networking.sendMoves(plan.getMoveList());
        }
    }
}
