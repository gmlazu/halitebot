import hlt.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MyBotv5 {

    public static void main(final String[] args) {

        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("GLazuV5");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);

        for (;;) {
            networking.updateMap(gameMap);

            Strategy strategy = new Strategy(gameMap);
            strategy.determineStrategy();

            Networking.sendMoves(strategy.getMoveList());
        }
    }
}
