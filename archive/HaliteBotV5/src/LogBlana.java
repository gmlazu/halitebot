import java.io.FileWriter;
import java.io.IOException;

public class LogBlana {

    private static FileWriter file = null;

    public static void log(String log) {
        try {
            file = new FileWriter("LogBlana.txt", true);
            file.write(log + System.getProperty("line.separator"));
            if (file != null) file.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
