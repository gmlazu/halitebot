import hlt.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyBotv4 {

    public static void main(final String[] args) throws IOException{
        LogBlana.log("Start of new log.");

        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("GLazuV4");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);
        LogBlana.log(initialMapIntelligence);

        final ArrayList<Move> moveList = new ArrayList<>();
        HashMap<Planet, ArrayList<Ship>> plannedDocking = new HashMap<>();

        for (;;) {
            moveList.clear();
            networking.updateMap(gameMap);

            for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
                if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                    continue;
                }

                ThrustMove newThrustMove = null;

                // iterate through every planet, ordered by distance
                for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
                    // we skip if entity is not a Planet
                    if (!(entity instanceof Planet)) continue;
                    Planet planet = (Planet) entity;


                    // If we have planets to conquer, we go to them
                    if (Utils.getAllOwnedPlanets(gameMap).size() < gameMap.getAllPlanets().size()) {
                        // we skip if planet is not ours
                        if (planet.isFull() || (planet.getOwner() != gameMap.getMyPlayerId() && planet.isOwned())) {
                            // if we had plans for the planet, remove them
                            if (plannedDocking.get(planet) != null)
                                plannedDocking.remove(planet);
                            continue;
                        }

                        // if we were not thrown out above, then we can make planning for this planet
                        LogBlana.log("Plan this planet for docking");
                        plannedDocking.put(planet, new ArrayList<>());

                        // if we have less plans for docking than the average docking spots for the map
                        if (plannedDocking.get(planet).size() < Utils.getAverageDockingSpots(gameMap) && Utils.getMaxDockingSpots(planet) >= Utils.getAverageDockingSpots(gameMap)) {
                            LogBlana.log("We can proceed docking for this planet.");
                            plannedDocking.get(planet).add(ship);
                            newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet, Constants.MAX_SPEED);
                            if (newThrustMove != null) {
                                if (ship.canDock(planet)) {
                                    LogBlana.log("dock to the planet...");
                                    moveList.add(new DockMove(ship, planet));
                                } else {
                                    LogBlana.log("moving towards planet...");
                                    moveList.add(newThrustMove);
                                }
                                break;
                            }
                        }
                        LogBlana.log("We don't have room to dock here.");
                    } else { // if we no longer have planets to conquer, attack the enemy's planet
                        LogBlana.log("We don't have planets to conquer, so we attack");
                        // do not attack planets owned by us
                        if (planet.getOwner() != gameMap.getMyPlayerId()) {
                            LogBlana.log("Current planet is not ours");
                            // this planet is not owned by us, so we send this ship to attack
                            newThrustMove = Navigation.navigateShipTowardsTarget(
                                    gameMap,
                                    ship,
                                    new Position(planet.getXPos(), planet.getYPos()),
                                    Constants.MAX_SPEED,
                                    true,
                                    Constants.MAX_NAVIGATION_CORRECTIONS,
                                    Math.PI / 180.0
                            );
                            if (newThrustMove != null) {
                                moveList.add(newThrustMove);
                                LogBlana.log("so we go to it");
                            }
                            break;
                        }
                    }
                }
            }
            Networking.sendMoves(moveList);
        }
    }
}
