import hlt.GameMap;
import hlt.Planet;

import java.util.ArrayList;

public class Utils {

    /**
     * Returns an ArrayList of Planets containing all owned (non-neutral) planets.
     * @param gameMap the game map
     * @return an ArrayList of Planets
     */
    public static ArrayList<Planet> getAllOwnedPlanets(GameMap gameMap) {
        ArrayList<Planet> ownedPlanets = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            if (planet.isOwned()) ownedPlanets.add(planet);
        }
        return ownedPlanets;
    }

    /**
     * Returns an ArrayList of Planets containing all planets owned by us.
     * @param gameMap the game map
     * @return an ArrayList of Planets
     */
    public static ArrayList<Planet> getMyOwnedPlanets(GameMap gameMap) {
        ArrayList<Planet> ownedPlanets = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            if (planet.getOwner() == gameMap.getMyPlayerId()) ownedPlanets.add(planet);
        }
        return ownedPlanets;
    }

    /**
     * @param planet the planet for which you want the max docking spots
     * @return max docking spots
     */
    public static int getMaxDockingSpots(Planet planet) {
        double planetRadius = planet.getRadius();

        return (int) Math.ceil(planetRadius / 3);
    }

    public static int getAverageDockingSpots(GameMap gameMap) {
        double averageDockingSpots = 0;
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            averageDockingSpots += getMaxDockingSpots(planet);
        }

        return (int) Math.floor(averageDockingSpots / gameMap.getAllPlanets().values().size());
    }
}
