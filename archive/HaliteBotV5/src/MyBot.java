import hlt.*;

import java.util.*;

public class MyBot {

    private final int DESIRED_SHIPS = 30;
    private final int DESIRED_DOCKED_SHIPS = 2;

    Random random = new Random(System.currentTimeMillis());

    private final Networking networking;
    private final GameMap gameMap;

    final ArrayList<Move> moveList = new ArrayList<>();
    final ArrayList<Planet> plannedPlanets = new ArrayList<>();

    final List<Planet> closestEmptyPlanets = new ArrayList<>();
    final List<Planet> closestFriendlyPlanets = new ArrayList<>();
    final List<Ship> closestEnemyShips = new ArrayList<>();

    public MyBot(Networking networking, GameMap gameMap) {
        this.networking = networking;
        this.gameMap = gameMap;
    }

    private void sortNearbyEntities(Ship ship) {
        Map<Double, Entity> nearestEntities = gameMap.nearbyEntitiesByDistance(ship);

        closestEmptyPlanets.clear();
        closestFriendlyPlanets.clear();
        closestEnemyShips.clear();


        for (Map.Entry<Double, Entity> entry : nearestEntities.entrySet()) {
            Entity he = entry.getValue();

            if (he instanceof Planet) {
                Planet planet = (Planet) he;

                if (!planet.isOwned()) {
                    closestEmptyPlanets.add(planet);
                } else if (planet.getOwner() == gameMap.getMyPlayerId()) {
                    closestFriendlyPlanets.add(planet);
                }
            } else if (he instanceof Ship) {
                Ship shp = (Ship) he;

                if (shp.getOwner() != gameMap.getMyPlayerId()) {
                    closestEnemyShips.add(shp);
                }
            }

            //Log.log("From shipId: " + shipId + " to entity " + entry.getKey());
        }
    }

    private void moveAttackRandom(Ship ship) {
        Ship target = null;

        for (Ship s : gameMap.getAllShips()) {
            if (s.getOwner() != gameMap.getMyPlayerId()) {
                target = s;
                break;
            }
        }

        if (target == null) {
            return;
        }

        final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, target, Constants.MAX_SPEED);
        if (newThrustMove != null) {
            moveList.add(newThrustMove);
        }
    }


    private boolean moveToEmptyPlanet(Ship ship) {
        Collections.sort(closestFriendlyPlanets, new Comparator<Planet>() {
            @Override
            public int compare(Planet o1, Planet o2) {
                /*double d1 = Math.pow(o1.getXPos() - ship.getXPos(), 2) + Math.pow(o1.getYPos() - ship.getYPos(), 2);
                double d2 = Math.pow(o2.getXPos() - ship.getXPos(), 2) + Math.pow(o2.getYPos() - ship.getYPos(), 2);

                if (d1 >= 100) {
                    return 1;
                }

                if (d2 >= 100) {
                    return -1;
                }*/

                // The 2 planets are nearby so we choose the biggest one available
                return Double.compare(o1.getRadius(), o2.getRadius());
            }
        });

        for (Planet target : closestEmptyPlanets) {
            if (ship.canDock(target)) {
                moveList.add(new DockMove(ship, target));
                return true;
            }

            if (plannedPlanets.contains(target)) {
                continue;
            }

            final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, target, Constants.MAX_SPEED);
            if (newThrustMove != null) {
                moveList.add(newThrustMove);
                plannedPlanets.add(target);
                return true;
            }
        }

        //no more planets to go to
        return false;
    }


    private boolean moveToOurPlanet(Ship ship) {
        for (Planet target : closestFriendlyPlanets) {
            if (target.getDockedShips().size() >= target.getRadius() / 3) {
                return false;
            }

            if (ship.canDock(target)) {
                moveList.add(new DockMove(ship, target));
                return true;
            }

            if (plannedPlanets.contains(target)) {
                continue;
            }

            final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, target, Constants.MAX_SPEED);
            if (newThrustMove != null) {
                moveList.add(newThrustMove);
                plannedPlanets.add(target);
                return true;
            }
        }

        return false;
    }

    private boolean randomize(Ship ship) {
        if (random.nextFloat() > 0.3f) {
            return false;
        }

        if (moveToEmptyPlanet(ship)) {
            return true;
        }

        if (moveAttackEnemyShip(ship)) {
            return true;
        }

        if (moveToOurPlanet(ship)) {
            return true;
        }

        return false;
    }

    private boolean moveAttackEnemyShip(Ship mine) {
        for (Ship enemy : closestEnemyShips) {
            final ThrustMove newThrustMove = Navigation.navigateShipTowardsTarget(
                    gameMap,
                    mine,
                    mine.getClosestPoint(enemy),
                    Constants.MAX_SPEED,
                    true,
                    Constants.MAX_NAVIGATION_CORRECTIONS,
                    Math.PI/180.0
            );

            if (newThrustMove != null) {
                moveList.add(newThrustMove);
                return true;
            }
        }

        return false;
    }

    private boolean enoughShips() {
        return (closestFriendlyPlanets.size() > gameMap.getAllPlanets().size() / 1.5f);
    }

    public void playTurn(GameMap gameMap) {
        moveList.clear();
        plannedPlanets.clear();

        networking.updateMap(gameMap);


        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {

            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }


            sortNearbyEntities(ship);

            if (randomize(ship)) {
                continue;
            }

            if (/*enoughShips() &&*/ moveToEmptyPlanet(ship)) {
                continue;
            }

            if (gameMap.getMyPlayer().getShips().size() >= DESIRED_SHIPS) {
                if (moveAttackEnemyShip(ship)) {
                    continue;
                }
            }

            if (moveToOurPlanet(ship)) {
                continue;
            }

            moveAttackEnemyShip(ship);
        }
        Networking.sendMoves(moveList);
    }




    public static void main(final String[] args) {
        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("Bulan");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);

        MyBot bot = new MyBot(networking, gameMap);

        for (;;) {
            bot.playTurn(gameMap);
        }
    }
}
