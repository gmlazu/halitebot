import hlt.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Strategy {

    private GameMap gameMap;

    private ArrayList<Move> moveList = new ArrayList<>();

    HashMap<Planet, ArrayList<Ship>> plannedDocking = new HashMap<>();
    HashMap<Ship, Ship> plannedAttack = new HashMap<>();

    public Strategy(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    public void determineStrategy() {
        if (Utils.getAllOwnedPlanets(gameMap).size() < gameMap.getAllPlanets().size()) {
            mineStrategy();
        } else {
            attackStrategy();
        }
    }

    private void mineStrategy() {
        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            ThrustMove newThrustMove = null;

            // iterate through every planet, ordered by distance
            for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
                // we skip if entity is not a Planet
                if (!(entity instanceof Planet)) continue;
                Planet planet = (Planet) entity;

                // we skip if planet is not ours
                if (planet.isFull() || (planet.getOwner() != gameMap.getMyPlayerId() && planet.isOwned())) {
                    // if we had plans for the planet, remove them
                    if (plannedDocking.get(planet) != null)
                        plannedDocking.remove(planet);
                    continue;
                }

                // if we were not thrown out above, then we can make planning for this planet
                plannedDocking.computeIfAbsent(planet, k -> new ArrayList<>());

                // if we already have plans to dock at least the map average of docking spots
                // and we own less than 30% of the map
                // we search for another planet

                if (plannedDocking.get(planet).size() >= Utils.getAverageDockingSpots(gameMap) &&
                        Utils.getMyOwnedPlanets(gameMap).size() < (int) (gameMap.getAllPlanets().size() * 0.3)) {
                    continue;
                }

                // if we reached this line, that means we own more than 30% of map
                // or we have less plans to dock than the map average
                // in either case, we should verify if we have room to dock. if so, dock.

                if (planet.getDockedShips().size() < planet.getDockingSpots()) {
                    plannedDocking.get(planet).add(ship);
                    break;
                }
            }
        }
    }

    private void attackStrategy() {
        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            ThrustMove newThrustMove = null;

            for (int i = 0; i <= 1; i++) {
                boolean attackUndockedShips = i == 1;

                // iterate through every ship, ordered by distance
                for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
                    // we skip if entity is not a Planet
                    if (!(entity instanceof Ship)) continue;
                    Ship enemyShip = (Ship) entity;

                    // is it really an enemy ship?
                    if (enemyShip.getOwner() == gameMap.getMyPlayerId()) continue;

                    // we prioritise the docked ships first
                    if (!attackUndockedShips && enemyShip.getDockingStatus() == Ship.DockingStatus.Docked && plannedAttack.get(ship) == null) {
                        plannedAttack.put(ship, enemyShip);
                        break;
                    } else if (enemyShip.getDockingStatus() == Ship.DockingStatus.Undocked && plannedAttack.get(ship) == null) {
                        plannedAttack.put(ship, enemyShip);
                        break;
                    }
                }
            }
        }
    }


    public ArrayList<Move> getMoveList() {
        // iterate thrgough planned dockings
        for (Object o1 : plannedDocking.entrySet()) {
            Map.Entry plan = (Map.Entry) o1;
            Planet planet = (Planet) plan.getKey();
            ArrayList<Ship> ourShips = (ArrayList<Ship>) plan.getValue();

            for (Ship ship : ourShips) {
                ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet, Constants.MAX_SPEED);

                if (newThrustMove != null) {
                    if (ship.canDock(planet)) {
                        moveList.add(new DockMove(ship, planet));
                    } else {
                        moveList.add(newThrustMove);
                    }
                }
            }
        }

        // iterate through planned attacks
        for (Object o : plannedAttack.entrySet()) {
            Map.Entry plan = (Map.Entry) o;
            Ship ourShip = (Ship) plan.getKey();
            Ship enemyShip = (Ship) plan.getValue();

            ThrustMove newThrustMove = Navigation.navigateShipTowardsTarget(
                    gameMap,
                    ourShip,
                    new Position(enemyShip.getXPos(), enemyShip.getYPos()),
                    Constants.MAX_SPEED,
                    true,
                    Constants.MAX_NAVIGATION_CORRECTIONS,
                    Math.PI / 180.0
            );
            if (newThrustMove != null) {
                moveList.add(newThrustMove);
            }
        }

        return moveList;
    }
}
