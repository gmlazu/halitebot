package Lazuq;

import hlt.Entity;
import hlt.GameMap;
import hlt.Ship;

/**
 * The BalancedStrategy class extends the abstract class Strategy,
 * implementing a conquer-attack strategy upon init.
 */
public class BalancedStrategy extends Strategy {

    /**
     * Constructor for the BalancedStrategy class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public BalancedStrategy(GameMap gameMap, Plan plan) {
        super(gameMap, plan);
    }

    /**
     * This method initializes the current strategy by sending
     * 1/4 ships to attack, and 3/4 ships to mine.
     */
    @Override
    public void init() {
        int shipCount = 4;
        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
            // Docked ships are ignored
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            if (shipCount % 4 == 0) {
                sendShipToAttack(ship, false);
            } else {
                if (!sendShipToMine(ship, true))
                    sendShipToAttack(ship, true);
            }

            shipCount++;
        }
    }
}
