package Lazuq;

import hlt.Entity;
import hlt.GameMap;
import hlt.Planet;
import hlt.Ship;

/**
 * The SinglePlayer class extends the abstract class Strategy,
 * implementing a planet-conquering strategy for single-player games.
 */
public class SinglePlayerStrategy extends Strategy {

    /**
     * Constructor for the Lazuq.SinglePlayerStrategy class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public SinglePlayerStrategy(GameMap gameMap, Plan plan) {
        super(gameMap, plan);
    }

    /**
     * Overridden method for sending a ship to mine, but modified for the particular case
     * where we do not have to attack anyone
     * @param ship the ship to send
     * @param limitDocking whether the number of docks per planet should be limited
     * @return true on success, false on failure
     */
    @Override
    protected boolean sendShipToMine(Ship ship, boolean limitDocking) {
        // Docked ships are ignored
        if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
            return true;
        }

        // iterate through every planet, ordered by distance
        for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
            // we skip if entity is not a Planet
            if (!(entity instanceof Planet)) continue;
            Planet planet = (Planet) entity;

            // if we can dock on the nearest planet
            if (planet.getDockedShips().size() < 1 &&
                    (plan.getPlanetsBeingDocked().get(planet.getId()) == null ||
                            plan.getPlanetsBeingDocked().get(planet.getId()).size() < 1)) {
                return plan.scheduleDock(ship, planet);
            }
        }

        return false;
    }

    /**
     * This method initializes the current strategy by sending
     * undocked ships to mine. If this action fails, the ships are
     * instead sent to attack.
     */
    @Override
    public void init() {
        // iterate through every ship of ours
        for (Ship ship : gameMap.getMyPlayer().getShips().values()) {
            // skip docked ships
            if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                continue;
            }

            // send the ship to mine
            this.sendShipToMine(ship, true);
        }
    }
}
