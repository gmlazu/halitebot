package Lazuq;

import hlt.*;

import java.util.ArrayList;

/**
 * Abstract strategy class from which derive the three
 * strategies that are being used for this bot.
 */
public abstract class Strategy {

    /**
     * The game map
     */
    protected GameMap gameMap;

    /**
     * This HashMap holds an ArrayList of Ships that will be docking to a Planet
     */
    protected Plan plan;

    /**
     * Constructor for the Lazuq.Strategy abstract class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public Strategy(GameMap gameMap, Plan plan) {
        this.gameMap = gameMap;
        this.plan = plan;
    }

    public abstract void init();

    /**
     * Sends a ship to dock and mine to the closest planet
     * @param ship the ship to send
     * @param limitDocking determines whether we should limit number of docks per planet.
     * @return true on success, and false on failure
     */
    protected boolean sendShipToMine(Ship ship, boolean limitDocking) {
        // Docked ships are ignored
        if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
            return true;
        }

        // iterate through every planet, ordered by distance
        for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
            // we skip if entity is not a Planet
            if (!(entity instanceof Planet)) continue;
            Planet planet = (Planet) entity;

            // determines the maximum ships we allow to dock on this planet
            int maxDocksPerPlanet = (Utils.getMyOwnedPlanets(gameMap).size() <= 3 && limitDocking) ? 1 : Utils.getAvailableDockingSpots(planet);

            // if we can dock on the nearest planet
            if (planet.getDockedShips().size() < maxDocksPerPlanet &&
                    (plan.getPlanetsBeingDocked().get(planet.getId()) == null ||
                            plan.getPlanetsBeingDocked().get(planet.getId()).size() < maxDocksPerPlanet)) {
                return plan.scheduleDock(ship, planet);
            }
        }

        return false;
    }

    /**
     * Sends a Ship to attack closest enemies.
     * @param ship the ship to send
     * @param allowDocking - determines whether the ship sent is allowed to mine a neutral planet it finds on the way to attack.
     */
    protected boolean sendShipToAttack(Ship ship, boolean allowDocking) {
        // even if we are in attack strategy, if we have a planet nearby
        // and if we allow docking on a nearby planet
        // and we can dock to it, do it.
        if (allowDocking) {
            for (Entity entity : gameMap.nearbyEntitiesByDistance(ship).values()) {
                // we skip if entity is not a Planet
                if (!(entity instanceof Planet)) continue;
                Planet planet = (Planet) entity;

                if (!ship.canDock(planet) || planet.isFull() || (planet.getOwner() != gameMap.getMyPlayerId() && planet.isOwned()))
                    continue;

                plan.scheduleDock(ship, planet);
                return true;
            }
        }

        // we get the closest enemy ship that wasn't planned for attack
        for (Ship enemyShip : Utils.getEnemyShipsByDistance(gameMap, ship, 3)) {
            if (plan.scheduleAttack(ship, enemyShip)) {
                return true;
            }
        }

        return false;
    }
}