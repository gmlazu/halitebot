package Lazuq;

import hlt.GameMap;
import hlt.Ship;

/**
 * The AttackStrategy class extends the abstract class Strategy,
 * implementing an attack strategy upon init.
 */
public class AttackStrategy extends Strategy {

    /**
     * Constructor for the AttackStrategy class
     * @param gameMap the game map
     * @param plan the main Plan object
     */
    public AttackStrategy(GameMap gameMap, Plan plan) {
        super(gameMap, plan);
    }

    /**
     * This method initializes the current strategy by sending
     * the ships to attack docked ships first, undocked ships after.
     */
    @Override
    public void init() {
        for (Ship ship : gameMap.getMyPlayer().getShips().values()) {
            sendShipToAttack(ship, true);
        }
    }
}
