package Lazuq;

import hlt.GameMap;

/**
 * Factory class to generate a type of Strategy to use this round.
 */
public class StrategyFactory {

    /**
     * Factory getter method to determine what strategy to use.
     * The AttackStrategy strategy will be chosen if there are no more neutral planets.
     * The ConquerStrategy strategy will be chosen if the game is 4-player.
     * The BalancedStrategy strategy will be chosen in any other case.
     * @param gameMap the game map
     * @param plan the main Plan object
     * @return the strategy to use this round
     */
    public static Strategy getStrategy(GameMap gameMap, Plan plan) {

        // if the game is single player, we implement the single player strategy
        if (gameMap.getAllPlayers().size() == 1)
            return new SinglePlayerStrategy(gameMap, plan);

        // if there are no more planets to conquer,
        // or if the game map has small planets, we attack
        if (Utils.getAllOwnedPlanets(gameMap).size() == gameMap.getAllPlanets().size() || (Utils.getAverageDistanceBetweenPlanets(gameMap) < 800))
            return new AttackStrategy(gameMap, plan);

        // if the game is 4 player, or if the distance between planets is small,
        // we use the conquer strategy until there are no more planets to conquer
        if (gameMap.getAllPlayers().size() == 4 || (Utils.getAverageDistanceBetweenPlanets(gameMap) > 800 && gameMap.getWidth() > 300 && gameMap.getHeight() > 200))
            return new ConquerStrategy(gameMap, plan);

        // in every other case, use balanced strategy
        return new BalancedStrategy(gameMap, plan);
    }
}
