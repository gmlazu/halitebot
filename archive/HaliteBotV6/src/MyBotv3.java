import hlt.*;

import java.io.IOException;
import java.util.ArrayList;

public class MyBotv3 {

    public static void main(final String[] args) throws IOException{

        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("GLazuV3");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);

        ArrayList<Planet> plannedPlanets = new ArrayList<>();

        final ArrayList<Move> moveList = new ArrayList<>();
        for (;;) {
            moveList.clear();
            networking.updateMap(gameMap);

            boolean shouldAttack = true;
            for (Planet planet : gameMap.getAllPlanets().values()) {
                if (!planet.isOwned()) {
                    shouldAttack = false;
                }
            }

            for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
                if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                    continue;
                }

                for (final Planet planet : gameMap.getAllPlanets().values()) {
                    if (planet.isOwned()) {
                        if (shouldAttack && planet.getOwner() != gameMap.getMyPlayerId()) {
                            final ThrustMove newThrustMove = Navigation.navigateShipTowardsTarget(
                                    gameMap,
                                    ship,
                                    new Position(planet.getXPos(), planet.getYPos()),
                                    Constants.MAX_SPEED,
                                    true,
                                    Constants.MAX_NAVIGATION_CORRECTIONS,
                                    Math.PI/180.0
                            );
                            if (newThrustMove != null) {
                                moveList.add(newThrustMove);
                            }

                            break;
                        } else {
                            continue;
                        }
                    }

                    if (ship.canDock(planet)) {
                        moveList.add(new DockMove(ship, planet));
                        break;
                    }

                    if(plannedPlanets.contains(planet)) continue;

                    final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet, Constants.MAX_SPEED/2);
                    if (newThrustMove != null) {
                        moveList.add(newThrustMove);
                    }

                    break;
                }
            }
            Networking.sendMoves(moveList);
        }
    }
}
